﻿
var UI = {


    init: function () {


        $("#left").on("click", function () {

            $("#panel").animate({ "left": "0px" }, 500);
            $("#panel").css("z-index", "120");

        })
        $(".exitpng").on("click", function () {

            $("#panel").animate({ "left": "-100%" }, 500);

        })


        $("#center").on("click", function () {
            $("#panelToday").css("width", "100%");
            $("#panelToday").css("height", "100vh");
            $("#panelToday").css("bottom", "-100%");
            $("#panelToday").animate({ "bottom": "0px" }, 500);
            $("#panelToday").css("z-index", "120");


        })

        $(".exitToday").on("click", function () {

            $("#panelToday").animate({ "bottom": "-100%" }, 500);

        })


        $("#right").on("click", function () {

            $("#panelWeek").css("width", "100%");
            $("#panelWeek").css("height", "100vh");
            $("#panelWeek").css("bottom", "-100%");
            $("#panelWeek").animate({ "bottom": "0px" }, 500);
            $("#panelWeek").css("z-index", "120");


        })

        $(".exitWeek").on("click", function () {

            $("#panelWeek").animate({ "bottom": "-100%" }, 500);

        })



        $("#bazaKlik").on("click", function () {
            $("#panel").animate({ "left": "-100%" }, 500);
            $("#panelBaza").animate({ "left": "0px" }, 500);
            $("#panelBaza").css("z-index", "120");

        })
        $(".exitBaza").on("click", function () {

            $("#panelBaza").animate({ "left": "-100%" }, 500);

        })

        $(".exitAlert").on("click", function () {

            $("#alert").css("display", "none");

            tab11 = ["A0", "A1", "A2", "A3"];
            tab22 = ["B0", "B1", "B2", "B3"];
            tab33 = ["C0", "C1", "C2", "C3"];

            for(var i=0; i<tab11.length;i++){
                $("#" + tab11[i]).css("opacity", "1")
            }

            for (var i = 0; i < tab22.length; i++) {
                $("#" + tab22[i]).css("opacity", "1")
            }


            for (var i = 0; i < tab33.length; i++) {
                $("#" + tab33[i]).css("opacity", "1")
            }

            check = {
                pos1: "no",
                pos2: "no",
                pos3: "no",
                pos4: "no",
            }


        })

        //animacje end
        function ajaxOK(response) {
            $("#alert").css("display", "inline");
            $("#alertData").text(response);
        }

        function ajaxError(response) {
            $("#alert").css("display", "inline");
            $("#alertData").text(response.responseText);
        }






        ////////////////////////// AJAX


        //dodanie tabeli
        $("#addTab").on("click", function () {

            // alert("dziala")
            $("#alert").css("display", "inline");


            //ajaxOK(response);
            var obj = {
                action: "create",
            }
            Database.methods.createTables(obj)
             .done(function (response) {
                 ajaxOK(response) // na razie alert
             })
             .fail(function (response) {
                 ajaxError(response)
             })




        })

        //usuniecie tabeli
        $("#delTab").on("click", function () {

            // alert("dziala")
            $("#alert").css("display", "inline");

            var obj = {
                action: "drop",
            }
            Database.methods.dropTables(obj)
             .done(function (response) {
                 ajaxOK(response) // na razie alert
             })
             .fail(function (response) {
                 ajaxError(response)
             })



        })


        //dodanie rekordu tabeli
        $("#addData").on("click", function () {

            // alert("dziala")
            $("#alert").css("display", "inline");

            var obj = {
                action: "insert",
            }
            Database.methods.inse(obj)
             .done(function (response) {
                 ajaxOK(response) // na razie alert
             })
             .fail(function (response) {
                 ajaxError(response)
             })



        })



        //usuniecie rekordu tabeli
        $("#delData").on("click", function () {

            // alert("dziala")
            $("#alert").css("display", "inline");

            var obj = {
                action: "delete",
            }
            Database.methods.del(obj)
             .done(function (response) {
                 ajaxOK(response) // na razie alert
             })
             .fail(function (response) {
                 ajaxError(response)
             })



        })





        function download() {
            // alert("dziala")
            $("#alert").css("display", "inline");

            var obj = {
                action: "download",
            }
            Database.methods.download(obj)
             .done(function (response) {
                 ajaxOK("Dane pobrane") // na razie alert

                 var odj = JSON.parse(response);



                 $.each(odj.godziny, function (i) {



                     var id = odj.godziny[i].id

                     var d = $("<div>")
                     d.addClass("Blok");
                     d.addClass("changeColor");
                     d.addClass("changeBackground");
                     d.css("float", "left")
                     d.css("margin-top", "10px")
                     d.text(id);
                     $("#datah").append(d)


                     /////

                     var odG = odj.godziny[i].odG
                     var odM = odj.godziny[i].odM


                     var f = $("<button>")
                     f.addClass("idBlok");
                     f.addClass("from");
                     f.addClass("changeColor");
                     f.attr("id", "odG" + i)
                     f.attr("value", i + 1)
                     f.css("float", "left")
                     f.css("margin-top", "10px")
                     f.text(odG + " : " + odM);
                     $("#datah").append(f)


                     /////



                     var doG = odj.godziny[i].doG
                     var doM = odj.godziny[i].doM

                     var j = $("<button>")
                     j.addClass("idBlok");
                     j.addClass("too");
                     j.addClass("changeColor");
                     j.attr("id", "doG" + i)
                     j.attr("value", i + 1)
                     j.css("float", "left")
                     j.css("margin-top", "10px")
                     j.css("margin-left", "20%")
                     j.text(doG + " : " + doM);
                     $("#datah").append(j)




                 })



             })
             .fail(function (response) {
                 ajaxError(response)
             })




            ////////////////////////////// DAY


            Database.methods.downloadx(obj)
             .done(function (response) {
                 ajaxOK("Dane pobrane") // na razie alert

                 var odj = JSON.parse(response);



                 $.each(odj.Today, function (i) {



                     //var id = odj.godziny[i].id

                     var d = $("<div>")
                     d.addClass("dayBlok");
                     d.addClass("changeColor");
                     d.addClass("changeBackground");
                     d.css("float", "left")
                     d.text(i+1);
                     $("#mainDay").append(d)


                     /////

                     var subjectLongName = odj.Today[i].subjectLongName



                     var f = $("<button>")
                     f.addClass("daySubject");
                     f.addClass("changeColor");
                     f.addClass("changeBackground");
                     f.attr("id", "przedmiot" + i)
                     f.css("float", "left")
                     f.text(subjectLongName);
                     $("#mainDay").append(f)


                     /////



                     var nr = odj.Today[i].nr


                     var j = $("<button>")
                     j.addClass("dayNr");
                     j.addClass("changeColor");
                     j.addClass("changeBackground");
                     j.attr("id", "numerSali" + i)
                     j.css("float", "left")
                     j.text("sala : " + nr);
                     $("#mainDay").append(j)




                 })



             })
             .fail(function (response) {
                 ajaxError(response)
             })

            for (var i = 1; i < 15; i++) {
                var c = $("<div>")
                var cc = $("<div>")
                c.addClass("weekBlok");
                c.css("float", "left");
                c.css("clear", "both");
                if (i == 1) {
                    c.css("margin-top", "30px")
                }
                if (i == 14) {
                    c.css("height", "115px")
                }
                c.addClass("changeColor");
                cc.text(i);
                cc.css("margin-top","20px");
                c.append(cc)
                $("#mainWeek").append(c)

            }
            
            
            for (var i = 1; i < 71; i++) {
                var q = $("<button>")
                q.addClass("lesson");
                q.attr("id", i)
                if (i < 15)
                {
                    q.css("left", "16.666%");
                    q.css("top", i * 70 + "px");
                }
                else if (i > 14 && i < 29)
                {
                    q.css("left", "33.332%");
                    q.css("top", (i - 14) * 70 + "px");
                }
                else if (i > 28 && i < 43)
                {
                    q.css("left", "49.998%");
                    q.css("top", (i - 28) * 70 + "px");
                }
                else if (i > 42 && i < 57)
                {
                    q.css("left", "66.664%");
                    q.css("top", (i - 42) * 70 + "px");
                }
                else if (i > 56 && i < 71)
                {
                    q.css("left", "83.33%");
                    q.css("top", (i - 56) * 70 + "px");
                }

                $("#mainWeek").append(q)

            }
            
            ////////////////////////        WEEK
            
            Database.methods.downloadxx(obj)
             .done(function (response) {
                 ajaxOK("Dane pobrane") // na razie alert

                 var odj = JSON.parse(response);



                 $.each(odj.Week, function (i) {



                     /////

                     var subjectShortName = odj.Week[i].subjectShortName
                     var nr = odj.Week[i].nr


                     var f = $("<span>")
                     f.addClass("weekSubject");
                     f.attr("id", "weekSubject" + i)
                     f.css("float", "left")
                     f.css("clear", "both")
                     f.addClass("changeColor");
                     f.text(subjectShortName);
                     $("#"+(i+1)).append(f)

                     var ss = $("<br>")
                     $("#" + (i + 1)).append(ss)

                     var g = $("<span>")
                     g.addClass("weekSubject");
                     g.attr("id", "weekRoom" + i)
                     g.css("float", "left")
                     g.addClass("changeColor");
                     g.css("clear", "both")
                     g.text(nr);
                     $("#" + (i + 1)).append(g)


                 })



             })
             .fail(function (response) {
                 ajaxError(response)
             })
             

        }


        //////////////////////////////
        //////////////////////////////
        //////////////////////////////


        //Pobranie danych
        $("#download").on("click", function () {

            
            download();


        })





        $(".exitGodziny").on("click", function () {

            $("#panelGodziny").animate({ "bottom": "-100%" }, 500);

        })



        $("#godziny").on("click", function () {

            $("#panelGodziny").css("width", "100%");
            $("#panelGodziny").css("height", "100vh");
            $("#panelGodziny").css("bottom", "-100%");
            $("#panelGodziny").animate({ "bottom": "0px" }, 500);
            $("#panelGodziny").css("z-index", "120");




        })








        //////////////////////////////////////////
        /////////////wyborGodziny/////////////////
        //////////////////////////////////////////
















        $("#datah").on("click", ".idBlok", function () {
            $("#datah").css("display", "none")
            objData = {}

            indexADD = $(this).val();
            kk = $(this).attr("id")
            
            //console.log(indexADD);
            objData.id = indexADD;
            //console.log(objData);


            $("#wyborGodziny").css("display", "inline");
            //console.log(tabG.length)

            var a = $("<center>")
            a.text("Godziny");
            $("#hours").append(a)
            for (var i = 0; i < tabG.length; i++) {
                var d = $("<button>")
                d.attr("type", "button");
                d.addClass("oneH");
                d.attr("value", tabG[i]);
                d.addClass("changeColor");
                d.addClass("changeBackground");
                d.text(tabG[i]);
                $("#hours").append(d)
                

            }

            var aa = $("<center>")
            aa.text("Minuty");
            $("#minuty").append(aa)
            for (var i = 0; i < tabM.length; i++) {
                var d = $("<button>")
                d.attr("type", "button");
                d.addClass("oneM");
                d.addClass("changeColor");
                d.addClass("changeBackground");
                d.attr("value", tabM[i]);
                d.text(tabM[i]);
                $("#minuty").append(d)


            }


            if ($(this).hasClass("from") == true) {
                //console.log("from");
                objData.action = "setFrom";
                //$(this).off("click")


                $("#hours").on("click", ".oneH", function () {

                    
                    index1 = $(this).val();
                    //console.log(index1);
                    objData.odG = index1;
                    //console.log(objData);

                    if (index1 < 10) index1 = "0" + index1;
                    $("#mainH").text(index1);

                    $("#"+kk).text(index1)



                })








                $("#minuty").on("click", ".oneM", function () {

                    index2 = $(this).val();
                    //console.log(index2)
                    objData.odM = index2;
                    //console.log(objData);

                    if (index2 < 10) index2 = "0" + index2;
                    $("#mainM").text(index2);

                    $("#" + kk).text(index1+" : "+index2)
                })









            } else if ($(this).hasClass("too") == true) {
                //console.log("too")
                objData.action = "setToo";


                //$(".oneH").off("click")
                $("#hours").on("click", ".oneH", function () {

                    index3 = $(this).val();
                    //console.log(index3);
                    objData.doG = index3;
                    //console.log(objData);

                    if (index3 < 10) index3 = "0" + index3;
                    $("#mainH").text(index3);

                    $("#" + kk).text(index3)

                })




                $("#minuty").on("click", ".oneM", function () {

                    index4 = $(this).val();
                    //console.log(index4);
                    objData.doM = index4;
                    //console.log(objData);

                    if (index4 < 10) index4 = "0" + index4;
                    $("#mainM").text(index4);

                    $("#" + kk).text(index3 + " : " + index4)

                })



                    



            }




        })








        $(".exitWyborGodziny").on("click", function () {
            objData = {}
            //console.log(objData)
            $("#wyborGodziny").css("display", "none");
            $("#hours").empty();
            $("#minuty").empty();
            $("#mainM").text("00");
            $("#mainH").text("00");
            //update();
            $("#datah").css("display", "block")

        })
        
        
        $(".okWyborGodziny").on("click", function () {

            er = $("#mainM").text();
            er2 = $("#mainH").text();
            //console.log(er)
            //console.log(er2)
            if (er == 00 || er2 == 00) {
                //console.log("error")
                $("#alert").css("z-index", 200)
                ajaxOK("Brak danych !!!")

            }else{


            $("#wyborGodziny").css("display", "none");
            $("#datah").css("display", "block")
            $("#hours").empty();
            $("#minuty").empty();
            
            var obj = objData
            //console.log(obj)
            
            Database.methods.set(obj)
             .done(function (response) {
                 ajaxOK("Dane zaktualizowane") // na razie alert
             })
             .fail(function (response) {
                 ajaxError(response)
             })
            
            //update();

            objData = {}
            var obj = {}
            $("#mainM").text("00");
            $("#mainH").text("00");
            }

        })



        $("#mainH").on("click", function () {

            $("#mainH").css("border-right", "none");

        })







        //////////////////////////////////////////
        //////////////////////////////////////////
        //////////////////////////////////////////









    }
}


