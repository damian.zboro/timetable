﻿var colors = {


    init: function () {


        tabFont = ["#e8eaf6", "#c5cae9", "#8c9eff", "#536dfe", "#9fa8da", "#7986cb", "#5c6bc0", "#3f51b5", "#3949ab", "#303f9f", "#283593", "#1a237e"];
        tabTlo = ["#ffebee", "#ffcdd2", "#ff8a80", "#ef9a9a", "#e57373", "#ff5252", "#ef5350", "#f44336", "#e53935", "#d32f2f", "#c62828", "#b71c1c"];
        tabMenu = ["#fff8e1", "#ffecb3", "#ffe082", "#ffd54f", "#ffca28", "#ffc107", "#ffb300", "#ffa000", "#ff8f00", "#ff6f00", "#ef6c00", "#e65100"];

        

        setColor = {
            action: "setColor",
        }

        $("#colors").on("click", function () {
            $("#fontColor").empty();
            $("#tloColor").empty();
            $("#menuColor").empty();

            $("#panelColors").css("width", "100%");
            $("#panelColors").css("height", "100vh");
            $("#panelColors").css("bottom", "-100%");
            $("#panelColors").animate({ "bottom": "0px" }, 500);
            $("#panelColors").css("z-index", "120");


            for (var i = 0; i < tabFont.length; i++) {

                var q = $("<button>")
                q.attr("value",  tabFont[i] );
                q.addClass("colorss");
                q.css("background-color", tabFont[i]);
                $("#fontColor").append(q)
            }

            for (var i = 0; i < tabTlo.length; i++) {

                var q = $("<button>")
                q.attr("value", tabTlo[i]);
                q.addClass("colorss");
                q.css("background-color", tabTlo[i]);
                $("#tloColor").append(q)
            }

            for (var i = 0; i < tabMenu.length; i++) {

                var q = $("<button>")
                q.attr("value", tabMenu[i]);
                q.addClass("colorss");
                q.css("background-color", tabMenu[i]);
                $("#menuColor").append(q)
            }


        })

        $(".exitColors").on("click", function () {

            $("#panelColors").animate({ "bottom": "-100%" }, 500);
            setColor = {
                action: "setColor",

            }
            //console.log(setColor)

        })



        ///////////////////////////////////////////////

        $("#fontColor").on("click", ".colorss", function () {

            fColor = $(this).val();
            $(".changeColor").css("color", fColor);

            setColor.FontColor = fColor;
            //console.log(fColor);
        })


        $("#tloColor").on("click", ".colorss", function () {

            tColor = $(this).val();
            $(".changeBackground").css("background-color", tColor);
            $(".lesson").css("background-color", tColor);
            setColor.TloColor = tColor;
            //console.log(tColor);
        })


        $("#menuColor").on("click", ".colorss", function () {

            mColor = $(this).val();
            $(".changeMenu").css("background-color", mColor);

            $(".daySubject").hover(function () {
                $(this).css("background-color", mColor);
            }, function () {
                $(this).css("background-color", tColor);
            });

            $(".dayNr").hover(function () {
                $(this).css("background-color", mColor);
            }, function () {
                $(this).css("background-color", tColor);
            });

            $(".lesson").hover(function () {
                $(this).css("background-color", mColor);
            }, function () {
                $(this).css("background-color", tColor);
            });

            $(".idBlok").hover(function () {
                $(this).css("background-color", mColor);
            }, function () {
                $(this).css("background-color", "rgba(0, 0, 0, 0.8)");
            });

            
            setColor.MenuColor = mColor;
 
            //console.log(mColor);
        })
        /////////////////////////////////////////

        $("#saveColor").on("click", function () {

            
            setColor.userID = $("#saveColor").val();


            console.log(setColor)
            
            
            Database.methods.set(setColor)
             .done(function (response) {

                 $("#alert").css("display", "inline");
                 $("#alertData").text("Dane zaktualizowane");
                 $("#alert").css("z-index", "300");

             })
             .fail(function (response) {
                 alert(response)
             })
            
            setColor = {
                action: "setColor",
            }
            



        })



    }
}