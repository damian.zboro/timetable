﻿
var Database = {

    methods: {

        //utworzenie tabeli, przesyłam obiekt do wysłania Ajaxem

        createTables: function (obj) {
            return Ajax.send(obj, settings.urls.databaseUrl)
        },
        dropTables: function (obj) {
            return Ajax.send(obj, settings.urls.databaseUrl)
        },
        inse: function (obj) {
            return Ajax.send(obj, settings.urls.databaseUrl)
        },
        del: function (obj) {
            return Ajax.send(obj, settings.urls.databaseUrl)
        },
        download: function (obj) {
            return Ajax2.send(obj, settings.urls2.databaseUrl)
        },
        set: function (obj) {
            return Ajax.send(obj, settings.urls.databaseUrl)
        },
        downloadx: function (obj) {
            return AjaxX.send(obj, settings.urlsx.databaseUrl)
        },
        downloadxx: function (obj) {
            return AjaxXX.send(obj, settings.urlsxx.databaseUrl)
        },
        login: function (obj) {
            return AjaxLogin.send(obj, settings.urlsLogin.databaseUrl)
        },
        colors: function (obj) {
            return AjaxColor.send(obj, settings.urlsColors.databaseUrl)
        }
    }
}