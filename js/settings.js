﻿var settings = {
    init: function () {


        tabG = [7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20];
        tabM = [00, 05, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55];

        var stan = false
        $(".menu").on("click", function () {

            if (!stan) {
                $("#left").css("display", "inline")
                $("#center").css("display", "inline")
                $("#right").css("display", "inline")



                $("#left").css("height", "5vh")
                $("#center").css("height", "5vh")
                $("#right").css("height", "5vh")

                $("#left").css("width", "100%")
                $("#left").css("left", "0")
                $("#left").css("bottom", "10vh")

                $("#center").css("width", "100%")
                $("#center").css("left", "0")
                $("#center").css("bottom", "5vh")

                $("#right").css("width", "100%")
                $("#right").css("left", "0")
                $("#right").css("bottom", "0")


                $("#settingsIcon").css("width", "30%")
                $("#settingsIcon").css("float", "left")
                $("#settings").css("width", "70%")
                $("#settings").css("float", "left")

                $("#todayIcon").css("width", "30%")
                $("#todayIcon").css("float", "left")
                $("#today").css("width", "70%")
                $("#today").css("float", "left")

                $("#weekIcon").css("width", "30%")
                $("#weekIcon").css("float", "left")
                $("#week").css("width", "70%")
                $("#week").css("float", "left")


            } else {
                $("#left").css("display", "none")
                $("#center").css("display", "none")
                $("#right").css("display", "none")
            }

            stan = !stan


        })




    },

    urls: {

        databaseUrl: "server/Database.aspx",

    },
    urls2: {

        databaseUrl: "server/Test.aspx",

    },
    urlsx: {

        databaseUrl: "server/day.aspx",

    },
    urlsxx: {

    databaseUrl: "server/week.aspx",

    },
    urlsLogin: {

        databaseUrl: "server/Register.aspx",

    },
    urlsColors: {

        databaseUrl: "server/colors.aspx",

    }
    

}