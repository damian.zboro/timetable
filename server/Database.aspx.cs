﻿using System;
using System.Data.SqlClient;
using System.Data;

public partial class server_Database : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Response.AppendHeader("Access-Control-Allow-Origin", "*");
        string connstr = "Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\\Database.mdf;Integrated Security = True; Connect Timeout = 30";
        SqlConnection conn = new SqlConnection(connstr);


        switch (Request["action"])
        {
            case "create":

                try
                {
                    conn.Open();

                    //tab h
                    string sql = "CREATE TABLE godziny (id INTEGER, odG VARCHAR(20), doG VARCHAR(20), odM VARCHAR(20), doM VARCHAR(20));";
                    SqlCommand command = new SqlCommand();
                    command.CommandText = sql;
                    command.Connection = conn;
                    command.ExecuteNonQuery(); // wykonanie

                    //tab day

                    string sql2 = "CREATE TABLE dni (id INTEGER IDENTITY(1,1), dayShortName VARCHAR(20), dayLongName VARCHAR(20))";
                    SqlCommand commandx = new SqlCommand();
                    commandx.CommandText = sql2;
                    commandx.Connection = conn;
                    commandx.ExecuteNonQuery();


                    //tab przedmioty

                    string sql3 = "CREATE TABLE przedmioty (id INTEGER IDENTITY(1,1), subjectShortName VARCHAR(20), subjectLongName VARCHAR(20))";
                    SqlCommand commandxx = new SqlCommand();
                    commandxx.CommandText = sql3;
                    commandxx.Connection = conn;
                    commandxx.ExecuteNonQuery();


                    //tab users

                    string sql4 = "CREATE TABLE users (id INTEGER IDENTITY(1,1), userName VARCHAR(20), userPasswd VARCHAR(255))";
                    SqlCommand commandxxx = new SqlCommand();
                    commandxxx.CommandText = sql4;
                    commandxxx.Connection = conn;
                    commandxxx.ExecuteNonQuery();


                    //tab users

                    string sql5 = "CREATE TABLE lekcje (id INTEGER IDENTITY(1,1), nr VARCHAR(20), dayFK INTEGER, hourFK INTEGER, subjectFK INTEGER, userFK INTEGER)";
                    SqlCommand commandxxxx = new SqlCommand();
                    commandxxxx.CommandText = sql5;
                    commandxxxx.Connection = conn;
                    commandxxxx.ExecuteNonQuery();

                    //tab colors

                    string sql6 = "CREATE TABLE colors (id INTEGER IDENTITY(1,1), userID VARCHAR(20), FontColor VARCHAR(20), MenuColor VARCHAR(20), TloColor VARCHAR(20))";
                    SqlCommand commandColor = new SqlCommand();
                    commandColor.CommandText = sql6;
                    commandColor.Connection = conn;
                    commandColor.ExecuteNonQuery();


                    Response.Write("Utworzono tabele");
                }
                catch (Exception ex)
                {
                    Response.Write(ex.Message);
                }
                finally
                {
                    conn.Close();

                }


                break;


            case "drop":

                try
                {
                    conn.Open();
                    string sqldrop = "DROP TABLE godziny; DROP TABLE dni; DROP TABLE przedmioty; DROP TABLE users; DROP TABLE lekcje; DROP TABLE colors;";
                    SqlCommand command2 = new SqlCommand();
                    command2.CommandText = sqldrop;
                    command2.Connection = conn;
                    command2.ExecuteNonQuery(); // wykonanie
                    Response.Write("Usunieto tabele");
                }
                catch (Exception ex)
                {
                    Response.Write(ex.Message);
                }
                finally
                {
                    conn.Close();

                }

                break;


            case "insert":

                try
                {
                    conn.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = conn;
                    for (int i = 1; i < 15; i++)
                    {
                        string sqlin = "INSERT INTO godziny (id, odG, doG, odM, doM) VALUES(" + i + ",'00','00', '00', '00')";
                        command.CommandText = sqlin;
                        command.ExecuteNonQuery(); // wykonanie
                    }

                    //dni
                    SqlCommand command2 = new SqlCommand();
                    command2.Connection = conn;
                    string sqlin2 = "INSERT INTO dni (dayShortName, dayLongName) VALUES('PN','poniedzialek');INSERT INTO dni (dayShortName, dayLongName) VALUES('WT','wtorek');INSERT INTO dni (dayShortName, dayLongName) VALUES('SR','sroda');INSERT INTO dni (dayShortName, dayLongName) VALUES('CZ','czwartek');INSERT INTO dni (dayShortName, dayLongName) VALUES('PT','piatek')";
                    command2.CommandText = sqlin2;
                    command2.ExecuteNonQuery();


                    //przedmioty
                    SqlCommand command3 = new SqlCommand();
                    command3.Connection = conn;
                    string sqlin3 = "INSERT INTO przedmioty (subjectShortName, subjectLongName) VALUES('POL','jezyk polski');INSERT INTO przedmioty (subjectShortName, subjectLongName) VALUES('MAT','matematyka');INSERT INTO przedmioty (subjectShortName, subjectLongName) VALUES('ANG','jezyk angielski');INSERT INTO przedmioty (subjectShortName, subjectLongName) VALUES('AK','aplikacje klienckie');INSERT INTO przedmioty (subjectShortName, subjectLongName) VALUES('WF','wychowanie fizyczne')";
                    command3.CommandText = sqlin3;
                    command3.ExecuteNonQuery();


                    //dni
                    SqlCommand command4 = new SqlCommand();
                    command4.Connection = conn;
                    string sqlin4 = "INSERT INTO users (userName, userPasswd) VALUES('name','40BD001563085FC35165329EA1FF5C5ECBDBBEEF')";
                    command4.CommandText = sqlin4;
                    command4.ExecuteNonQuery();


                    //lekcje

                    SqlCommand command5 = new SqlCommand();
                    command5.Connection = conn;
                    for (int i = 1; i < 71; i++)
                    {

                        if (i < 15)
                        {
                            string sqlin5 = "INSERT INTO lekcje (nr, dayFK, hourFK, subjectFK, userFK) VALUES('222', '1', " + i + ", '1', '1')";
                            command5.CommandText = sqlin5;
                            command5.ExecuteNonQuery();
                        }
                        else if (i > 14 && i < 29)
                        {
                            string sqlin5 = "INSERT INTO lekcje (nr, dayFK, hourFK, subjectFK, userFK) VALUES('222', '2', " + (i - 14) + ", '2', '1')";
                            command5.CommandText = sqlin5;
                            command5.ExecuteNonQuery();
                        }
                        else if (i > 28 && i < 43)
                        {
                            string sqlin5 = "INSERT INTO lekcje (nr, dayFK, hourFK, subjectFK, userFK) VALUES('222', '3', " + (i - 28) + ", '3', '1')";
                            command5.CommandText = sqlin5;
                            command5.ExecuteNonQuery();
                        }
                        else if (i > 42 && i < 57)
                        {
                            string sqlin5 = "INSERT INTO lekcje (nr, dayFK, hourFK, subjectFK, userFK) VALUES('222', '4', " + (i - 42) + ", '4', '1')";
                            command5.CommandText = sqlin5;
                            command5.ExecuteNonQuery();
                        }
                        else if (i > 56 && i < 71)
                        {
                            string sqlin5 = "INSERT INTO lekcje (nr, dayFK, hourFK, subjectFK, userFK) VALUES('222', '5', " + (i - 56) + ", '5', '1')";
                            command5.CommandText = sqlin5;
                            command5.ExecuteNonQuery();
                        }

                    }


                    Response.Write("Dodano dane");
                }
                catch (Exception ex)
                {
                    Response.Write(ex.Message);
                }
                finally
                {
                    conn.Close();

                }
                break;


            case "delete":

                try
                {
                    conn.Open();
                    string sqldel = "DELETE FROM godziny; DELETE FROM dni; DELETE FROM przedmioty; DELETE FROM users; DELETE FROM lekcje; DELETE FROM colors;";
                    SqlCommand command4 = new SqlCommand();
                    command4.CommandText = sqldel;
                    command4.Connection = conn;
                    command4.ExecuteNonQuery(); // wykonanie
                    Response.Write("Usunieto dane");
                }
                catch (Exception ex)
                {
                    Response.Write(ex.Message);
                }
                finally
                {
                    conn.Close();

                }

                break;




            case "download":

                try
                {



                    conn.Open();
                    string sql = "SELECT * FROM godziny";
                    SqlDataAdapter da = new SqlDataAdapter(sql, conn);
                    DataTable dt = new DataTable();
                    da.Fill(dt);


                    Response.Write(dt.Rows.Count);      // ilość wierszy w tabeli
                    Response.Write(dt.Columns.Count);   // ilość kolumn w tabeli
                    Response.Write(dt.Columns[0].ColumnName); //nazwa 1 kolumny
                    Response.Write(dt.Rows[0][0]);  //wartość w 1 komórce 1 wiersza






                }
                catch (Exception ex)
                {
                    Response.Write(ex.Message);
                }
                finally
                {
                    conn.Close();

                }

                break;


            case "setToo":

                try
                {



                    conn.Open();
                    string sql = "UPDATE godziny SET doG =" + Request["doG"] + ", doM  =" + Request["doM"] + " WHERE id = " + Request["id"] + "";
                    SqlCommand command = new SqlCommand();
                    command.CommandText = sql;
                    command.Connection = conn;
                    command.ExecuteNonQuery();



                }
                catch (Exception ex)
                {
                    Response.Write(ex.Message);
                }
                finally
                {
                    conn.Close();

                }

                break;


            case "setFrom":

                try
                {



                    conn.Open();
                    string sql = "UPDATE godziny SET odG =" + Request["odG"] + ", odM  =" + Request["odM"] + " WHERE id = " + Request["id"] + "";
                    SqlCommand command = new SqlCommand();
                    command.CommandText = sql;
                    command.Connection = conn;
                    command.ExecuteNonQuery();



                }
                catch (Exception ex)
                {
                    Response.Write(ex.Message);
                }
                finally
                {
                    conn.Close();

                }

                break;



            case "setColor":

                try
                {



                    conn.Open();
                    // "INSERT INTO lekcje (nr, dayFK, hourFK, subjectFK, userFK) VALUES('222', '4', " + (i - 42) + ", '4', '1')";

                    string sql = "INSERT INTO colors (userID, FontColor, MenuColor, TloColor) VALUES('" + Request["userID"] + "','" + Request["FontColor"] + "','" + Request["MenuColor"] + "','" + Request["TloColor"] + "')";
                    SqlCommand command = new SqlCommand();
                    command.CommandText = sql;
                    command.Connection = conn;
                    command.ExecuteNonQuery();



                }
                catch (Exception ex)
                {
                    Response.Write(ex.Message);
                }
                finally
                {
                    conn.Close();

                }

                break;

        }










    }
}