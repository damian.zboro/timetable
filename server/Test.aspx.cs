﻿using System;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Text;

public partial class server_Test : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Response.AppendHeader("Access-Control-Allow-Origin", "*");
        string connstr = "Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\\Database.mdf;Integrated Security = True; Connect Timeout = 30";
        SqlConnection conn = new SqlConnection(connstr);


        switch (Request["action"])
        {

            case "download":

                try
                {

                    conn.Open();
                    string sql = "SELECT * FROM godziny";
                    SqlDataAdapter da = new SqlDataAdapter(sql, conn);
                    DataTable dt = new DataTable();
                    da.Fill(dt);

                    /*
                    Response.Write(dt.Rows.Count);      // ilość wierszy w tabeli
                    Response.Write(dt.Columns.Count);   // ilość kolumn w tabeli
                    Response.Write(dt.Columns[0].ColumnName); //nazwa 1 kolumny
                    Response.Write(dt.Rows[0][0]);  //wartość w 1 komórce 1 wiersza
                    Response.Write(dt.TableName);
                    */


                    //sb.Append("\""); // cudzysłów w napisie
                    //sb.AppendLine(); // nowa linia
                    //sb.Append("\t"); // tabulator

                    StringBuilder sb = new StringBuilder();
                    sb.Append("{").Append("\"").Append("godziny").Append("\"").Append(":").Append("\n").Append("\t").AppendLine("[");

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        sb.Append("\t").AppendLine().Append("{");




                        for (int j = 0; j < dt.Columns.Count; j++)
                        {

                            sb.Append("\"" + dt.Columns[j].ColumnName + "\"" + ":" + "\"" + dt.Rows[i][j] + "\"");


                            if (j < dt.Columns.Count - 1)
                            {
                                sb.Append(",");
                            }
                            else if (j == dt.Columns.Count - 1)
                            {
                                sb.Append("");
                            }

                        }





                        if (i < dt.Rows.Count - 1)
                        {
                            sb.Append("},");
                        }
                        else if (i == dt.Rows.Count -1)
                        {
                            sb.Append("}");
                        }






                    }



                    sb.Append("\n").AppendLine().Append("\t").Append("]").Append("\n").Append("}");

                    Response.Write(sb.ToString());





                }
                catch (Exception ex)
                {
                    Response.Write(ex.Message);
                }
                finally
                {
                    conn.Close();

                }

                break;

        }


    }
}