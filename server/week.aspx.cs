﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;


public partial class server_week : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {


        Response.AppendHeader("Access-Control-Allow-Origin", "*");
        string connstr = "Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\\Database.mdf;Integrated Security = True; Connect Timeout = 30";
        SqlConnection conn = new SqlConnection(connstr);

        conn.Open();
        string sql = "SELECT przedmioty.subjectShortName, lekcje.nr FROM lekcje LEFT JOIN przedmioty  ON (lekcje.subjectFK = przedmioty.id) LEFT JOIN users ON (lekcje.userFK = users.id) WHERE lekcje.userFK = 1";
        SqlDataAdapter da = new SqlDataAdapter(sql, conn);
        DataTable dt = new DataTable();
        da.Fill(dt);


        StringBuilder sb = new StringBuilder();
        sb.Append("{").Append("\"").Append("Week").Append("\"").Append(":").Append("\n").Append("\t").AppendLine("[");

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            sb.Append("\t").AppendLine().Append("{");




            for (int j = 0; j < dt.Columns.Count; j++)
            {

                sb.Append("\"" + dt.Columns[j].ColumnName + "\"" + ":" + "\"" + dt.Rows[i][j] + "\"");


                if (j < dt.Columns.Count - 1)
                {
                    sb.Append(",");
                }
                else if (j == dt.Columns.Count - 1)
                {
                    sb.Append("");
                }

            }





            if (i < dt.Rows.Count - 1)
            {
                sb.Append("},");
            }
            else if (i == dt.Rows.Count - 1)
            {
                sb.Append("}");
            }






        }



        sb.Append("\n").AppendLine().Append("\t").Append("]").Append("\n").Append("}");

        Response.Write(sb.ToString());


        conn.Close();

    }
}